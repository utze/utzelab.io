---
layout: page
title: "Sumario"
permalink: /about/
image: images/ OCOTSILABO02.png
comments: true 
---

![#piloto]({{ site.baseurl }}/images/Pinky_Cerebro_corregido.png)


Somos Paúl y Benjamín e intentamos recrear el ambiente de un post ágape después de una tenida. 

Suponemos que lo tratado en la misma quedó rebotando en el ambiente sin agotarse. 

Ese suele ser el momento en que los hermanos abordamos el tema con sus variaciones, que pueden ser muchas. 

Somos dos masones iniciados en la Logia Arauco N°20 de la GLEDE en Quito Ecuador y llevamos en las masonerías ecuatorianas muchos años. 

Siempre añorando una época del siglo XX en que nuestra Logia Madre fue intelectualmente suscitadora y formalmente irreverente.
