---
layout: page
title: "PROXIMAMENTE"
permalink: /next/
image: images/ojo.jpg
comments: true 
---

![#piloto]({{ site.baseurl }}/images/ojo.jpg)


**1° TEMPORADA 2021**

1. **EL NO PODER**
2. **LA BUENA MUERTE**
3. **LA VIDA ES UN VODEVIL**
4. **MINDFULNESS, AUTOAYUDA, ZEN, LIDERAZGO**
5. **ANARQUISTAS**
6. **MAGIA**
7. **HABLANDO DE MUJERES Y MISOGINIAS**
8. **CAMBIO CLIMÁTICO**
9. **REALISMO POLITICO**
10. **ACUMULADORES**
11. **VIRTUALIDAD**
12. **LA CENSURA**
13. **ESPIRITUALIDAD**
14. **TODO SOBRE LA MUERTE**
15. **LA IGUALDAD**
16. **INTELIGENCIAS**

**2° TEMPORADA 2021**

1. La década pandémica
2. Emprendimiento, empleo y flexibilización laboral
3. Realidad virtual y aumentada
4. La propiedad intelectual, el copyleft, creative commons, la piratería
5. La literatura y los libros
6. Escuelas iniciáticas y masonería
7. El suicidio y la muerte
8. Los cuatro amores: del ágape al agapé
9. Drogas recreativas y adicciones
10. Tecnología y conocimiento aplicado
11. El humanismo (proyecto humano)
12. Innovación, creación, creatividad y monetización
13. El capitalismo: vence pero no convence vs. el comunismo: convence pero no vence
14. La música
15. Big data, Smart data e Inteligencia Artificial
16. El epicureísmo, el hedonismo y el estoicismo
