---
layout: post  
title: "T1E1 EL TEATRO Y SUS CONSECUENCIAS"  
date: 2021-11-30  
categories: podcast  
image: images/T1E2_EL_TEATRO.jpeg
podcast_link: ia803402.us.archive.org/18/items/e-1-t-1-el-teatro/E1T1_EL_TEATRO.mp3
tags: [teatro, prepucio, vodevil]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/T1E2_EL_TEATRO.jpeg)

<audio controls>
    <source src="https://ia803402.us.archive.org/18/items/e-1-t-1-el-teatro/E1T1_EL_TEATRO.mp3" type="audio/mpeg">
    <script async data-type="track" data-hash="ePDBOReWZ5" data-track="3" src="https://app.fusebox.fm/embed/player.js"></script>
</audio>

**T1E1 EL TEATRO Y SUS CONSECUENCIAS**

**Prehistoria del Teatro, teatro ritual, teatro religioso.-**

El teatro como representación se origina en la ritualística religiosa que es el simbolismo en acción y tiene como finalidad causar un impacto numinoso (con lo trascendente) entre sus actores y espectadores. Se tiene referencias que en el antiguo Egipto (800 años antes de nuestra era), ya se representaban dramas acerca de la muerte y resurrección de Osiris, para lo cual se utilizaban máscaras y atuendos predeterminados. 

**Orígenes del Teatro.-**

Teatro proviene del griego ‘théatron’ que significa “lugar para contemplar” y sus elementos más característicos son  el escenario, el  atrezo, un guión, una secuencia de escenas, la actuación y los actores. El teatro en ese sentido vendría a ser un género literario que representa historias frente a un público combinando diálogos o monólogos, con acciones,  gestos, sonidos o música incidental, escenarios, decoración y espectáculo. De ahí que el teatro es el arte dramático por excelencia.

**El teatro griego.-**

El teatro en la antigua Grecia tiene sus raíces en los ritos órficos y en los festivales celebrados por la vendimia y el solsticio de verano al dios Dioniso (siglo VI a.C.), estas ceremonias colectivas iban acompañadas de Ditirambos (cantos y danzas rituales). Posteriormente comenzaron las representaciones dramáticas, ejecutadas en las plazas de los pueblos ante un público que incluían a un actor y un coro (bardos, aedas y rapsodas). 

Ya en el siglo V a.C., en la Grecia clásica comienzan a representarse los modelos clásicos de la tragedia y la comedia con dramaturgos profesionales como Esquilo y Sófocles,  estos son los creadores de los arquetipos de los grandes dramas que hasta ahora conocemos.  

En Roma se siguió con la tradición griega y en el imperio romano el teatro era parte de la cotidianidad por lo que diseñó e implementó toda una innovación ingenieril para su puesta en escena: la cávea o graderío, el púlpitum, etc.

Posteriormente en la Edad Medieval el teatro fue adquiriendo relevancia con los grupos ambulantes e itinerantes, donde destaca la figura de los juglares y los trovadores 

**El teatro Isabelino.-**

Sin duda es Shakespeare el gran innovador del teatro, creando todos los paradigmas del hombre moderno real, según Harold Bloom: “Antes de Shakespeare había arquetipos; después de Shakespeare hubo personajes, hombres y mujeres capaces de cambiar, con personalidades absolutamente individualizadas”. La extraordinaria inteligencia de Hamlet, la perturbada imaginación de Macbeth, el ingenio de Falstaff, la capacidad de afecto de Lear, la teatralidad de Cleopatra, el genio de Yago para manipular la vida de los demás, son la demostración de lo que significa ser un ‘ser humano’ y lo universal de estos pensamientos, sentimientos y acciones en nuestra cotidianidad.

Tal era la grandeza creativa, psicoanalítica y literaria de Shakespere que es muy difícil creer que un ser humano cualquiera, haya logrado un genio tan trascendente que como anécdota desde la época hasta ahora aún existe, persiste, una especie de secta (antistratfordianos) que proclama que Shakespeare no existió, sino que sus obras fueron creadas por Francis Bacon y/o Christopher Marlowe.   

**El teatro simbolista.-**

Es considerado como una de las innovaciones del teatro en el siglo XIX, su origen es francés, a raíz de la poesía romántica y simbolista de autores como Baudelaire, Rimbaud, Verlaine, Mallarmé, encontrando en el arte una forma de expresión mediante el uso sutil del lenguaje simbolizado, es decir, se vale de símbolos para buscar el conocimiento intelectivo y la expresión conceptual. Tal vez su representante más destacado sea Maurice Maeterlink.

**El teatro existencialista.-**

Denominado también teatro del compromiso social, representan diferentes puntos de vista sobre la creación, naturaleza, existencia del ser humano y la responsabilidad que conlleva sus elecciones racionales e irracionales. Tiene como grandes referentes a Jean Paul Sartre y Albert Camus.

Una de sus derivaciones es el teatro del absurdo, cuyos principales creadores son Samuel Beckett, Jean Genet y Eugene Ionesco, se caracteriza por argumentos aparentemente carentes de significado, diálogos repetitivos y falta de secuencia dramática que a menudo crean una atmósfera onírica, cuestiona la sociedad y al ser humano, y a través del humor negro, la incoherencia, el disparate y lo ilógico esconden su compromiso riguroso por el arte.

**El teatro de la crueldad.-**

Es un tipo de teatro propuesto por el francés Antonin Artaud, cuyo objetivo es sorprender e impresionar a los espectadores mediante situaciones impactantes e inesperadas. La obra dramática debe dejar una huella en el espectador mediante el deslumbramiento del espectáculo teatral.

**El método Stanislavski.-**

Lee Strasberg, en su academia ubicada en el ‘Studio 54’ de New York, populariza este método creado por Konstantin Stanislavski, que es “un conjunto de técnicas y ensayos que buscan fomentar actuaciones teatrales sinceras y emocionalmente expresivas”, es decir, los actores hacen uso de las experiencias de sus propias vidas para acercarlos a la experiencia de sus personajes, para ello deberán activar una serie de motores internos que les hacen transmitir al público sus emociones (porque son reales).

**La imagen y el espacio de representación.-**

Vivimos en una cultura predominantemente visual, la imagen es una representación de nuestras percepciones (visuales, auditivas, sensitivas, etc.), una construcción que efectúa la psiquis para sintetizar la realidad y activar cargas de impulsos que den lugar a acciones en el mundo. En otras palabras la imagen es el modo en que la conciencia estructura las percepciones y sensaciones que provienen de los sentidos y la memoria a través de impulsos que son transformados y sintetizados en forma de respuestas. Las imágenes movilizan energía psíquica que puede tener carácter catártico o transferencial de lo cual se obtiene un registro de índole doloroso, placentero, integrador, contradictorio o neutro. 

El espacio de representación es la “pantalla” en la que se representa las imágenes visuales, táctiles, olfativas, auditivas, gustativas, cenestésicas y kinestésicas. Su principal función es delimitar la frontera entre el punto de vista observador y el mundo, permitiendo las acciones en una determinada dirección y con un objetivo. El nivel de conciencia y el límite del espacio de representación coincidirá con el límite del cuerpo en vigilia (particularmente el rostro  concentra la mayoría de sentidos) y en cuando dormimos aumenta considerablemente su volumen aumentando el registro del intracuerpo.

**El teatro y en la actualidad.-**

Sin duda el teatro y todas sus formas intentan representar al mundo, al ser humano y sus interacciones, en la actualidad existe la necesidad de encontrar nuevas formas de representación de lo real para la situación actual. Muchos expertos en las artes escénicas, a través de sus trabajos, proponen el surgimiento de formas dramáticas que se distancien de la lógica aristotélica y de la estructura tradicional de fabular y contar historias, acompañando dicha experimentación con el compromiso ético y político que pone énfasis y en tela de juicio el horizonte filosófico y teolológico de la forma clásica del drama. 

En este tipo de teatro, lo ilusorio, lo simbólico ya no es un mundo de ideas, sino una dimensión constitutiva de lo inexistente. Los personajes cada vez más pierden aquel principio constitutivo y  primordial que permite cohesionar su individualidad. Y ya sin identidad (sin saber quiénes son), terminan perdiendo su forma,  apoderándose en ellos un sinnúmero de contradicciones. A este falso recorrido existencial, el no-personaje intenta restablecerse de nuevo, sin lograrlo. El teatro actual ¿un horror?... Para el debate.




Cualquier cosita nos escribes a:

+ Correo: <unicornioazul@disroot.org>

y... ¡Listo! Ahí vemos.
